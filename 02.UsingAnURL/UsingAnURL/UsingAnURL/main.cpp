#include "stdio.h"
#include "XCamera.h"

int main()
{
	printf("Opening connection \n");
	XCHANDLE cam = XC_OpenCamera("gev://192.168.0.158"); //Opens a direct connection to the camera when using the custom URL

	if (!XC_IsInitialised(cam))
	{
		printf("Could not open a connection to cam1\n");
		return -1;
	}

	printf("Cameras initialized\n");
	printf("Cleanup\n");

	XC_CloseCamera(cam);


	return 0;
}