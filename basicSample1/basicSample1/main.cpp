#include "stdio.h" // C Standard Input/Output 라이브러리
#include "XCamera.h" // Xeneth SDK main header

int main()
{
	// 변수설정
	XCHANDLE handle = 0; // 카메라로 handle 값 설정
	ErrCode errorCode = 0; // SDK 함수에서 반환된 errorCodes를 저장하기 위해 사용함
	word *frameBuffer = 0; // 캡처한 프레임을 16비트 버퍼에 저장
	dword frameSize = 0; // raw image의 바이트 크기

	//카메라 감지
	printf("Opening connection to cam://0\n");
	handle = XC_OpenCamera("cam://0");

	//연결 초기화
	if (XC_IsInitialised(handle))
	{
		// 캡처 시작
		printf("Start capturing.\n");
		if ((errorCode = XC_StartCapture(handle)) != I_OK)
		{
			printf("Could not start capturing, errorCode: %lu\n", errorCode);
		}
		else if (XC_IsCapturing(handle)) 
		{
			// 최초 framesize 측정
			frameSize = XC_GetFrameSize(handle);

			// 16비트 버퍼에 초기화
			frameBuffer = new word[frameSize / 2];

			// 카메라 Grab
			printf("Grabbing a frame.\n");
			if ((errorCode = XC_GetFrame(handle, FT_NATIVE, XGF_Blocking, frameBuffer, frameSize)) != I_OK)
			{
				printf("Problem while fetching frame, errorCode %lu", errorCode);
			}
		}
	}
	else
	{
		printf("Initialization failed\n");
	}

	// 청소

	// 계속 캡처 진행 시
	if (XC_IsCapturing(handle))
	{
		//캡처 멈춤
		printf("Stop capturing.\n");
		if ((errorCode = XC_StopCapture(handle)) != I_OK)
		{
			printf("Could not stop capturing, errorCode: %lu\n", errorCode);
		}
	}

	// handle이 카메라로 계속 초기화 시
	if (XC_IsInitialised(handle))
	{
		printf("Closing connection to camera.\n");
		XC_CloseCamera(handle);
	}

	printf("Clearing buffers.\n");
	if (frameBuffer != 0)
	{
		delete[] frameBuffer;
		frameBuffer = 0;
	}

	return 0;
}
